variable "instance_name" {
  type        = string
  default     = ""
  description = "Name of the instance."
}
