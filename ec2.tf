####################
# RESOURCES
####################

resource "aws_instance" "example" {
  ami           = "ami-08111162"
  instance_type = "t2.micro"
  
  tags = {
      Name = var.instance_name
  }
}
